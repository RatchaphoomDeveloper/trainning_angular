import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit {
  @Input() name: string = '';
  // ตัวอย่างสำกรับ angular 4
  // @Output('setName') name_s:EventEmitter = new EventEmitter();
  @Output() setName: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {
    this.setName.emit('Angular 10')
  }
}
