import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'changename'
})
export class ChangenamePipe implements PipeTransform {

  transform(value: string): string {
    return `Mr.${value.split(" ")[0]}`;
  }

}
