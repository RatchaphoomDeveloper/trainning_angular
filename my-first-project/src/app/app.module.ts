import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Home2Component } from './home2/home2.component';
import { Assignment1Component } from './assignment1/assignment1.component';
import { ChangenamePipe } from './changename.pipe';
import { TestpipePipe } from './pipes/testpipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Home2Component,
    Assignment1Component,
    ChangenamePipe,
    TestpipePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
