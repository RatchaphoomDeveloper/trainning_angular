import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AssignmentMessage } from './AssignmentFunction/AssignmentEnum';
import { ChangeYearFunction } from './AssignmentFunction/AssignmentMainFunction';

@Component({
  selector: 'app-assignment1',
  templateUrl: './assignment1.component.html',
  styleUrls: ['./assignment1.component.scss']
})
export class Assignment1Component implements OnInit {

  userForm = new FormGroup({
    name:new FormControl('',Validators.required),
    lastname:new FormControl('',Validators.required),
    old:new FormControl('',Validators.required)
  })

  dataValue:Date = new Date()

  valueReturn:string = ''
  valuePush:Array<any> = []

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit():void{
      let valueP = []
      if(this.userForm.invalid){
        alert(AssignmentMessage.ERROR)
      }else{
        this.valueReturn = ChangeYearFunction(this.userForm.get('name')?.value,this.userForm.get('lastname')?.value,this.userForm.get('old')?.value)
        this.valuePush.push({data:this.valueReturn})
        this.userForm.reset()
      }
      console.log(this.valuePush)
  }

}
