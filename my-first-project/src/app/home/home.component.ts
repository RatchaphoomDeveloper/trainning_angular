import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Message, WEB_URL } from './functions/enumFunction';
import { changeNameValue } from './functions/mainFunction';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  name: string = '';
  arr: any = [
    {
      id: 1,
      workname: 'risk1',
    },
    {
      id: 2,
      workname: 'risk2',
    },
    {
      id: 3,
      workname: 'ระบบท่อ',
    },
    {
      id: 4,
      workname: 'ระบบแก๊ส',
    },
  ];
  // flag:boolean = true;

  userGroupForm = new FormGroup({
    firstname: new FormControl('', [
      Validators.maxLength(16),
      Validators.required,
    ]),
    lastname: new FormControl('', [
      Validators.maxLength(16),
      Validators.required,
    ]),
  });

  constructor() {}

  ngOnInit(): void {
    // console.log(this.flag)
    console.log(Message.SUCCESS)
    console.log(Message.ERROR)
    console.log(Message.ALERT)
    console.log(WEB_URL)

    let chk = changeNameValue('Ratchaphoom','Mr');
    this.name = this.checkName('Ratchaphoom');
  }

  checkName(firstname: string): string {
    let checker =
      firstname === 'Ratchaphoom' ? 'Fluke' : `I don't know, Who r u?`;
    return checker;
  }

  setName(event: any) {
    console.log(event);
  }

  // onSubmit(f:NgForm){
  //   console.log(f.value)
  // }

  onSubmit() {
    console.log('firstname : ' + this.userGroupForm.get('firstname')?.value);
    console.log('lastname : ' + this.userGroupForm.get('lastname')?.value);
  }
}
