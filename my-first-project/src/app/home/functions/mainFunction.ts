export const changeNameValue = (name: string, typename: string): string => {
  return `${typename}.${name}`;
};
