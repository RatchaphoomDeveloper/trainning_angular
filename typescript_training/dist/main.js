"use strict";
// // let,var มันเปลี่ยนค่าได้
// // const มันเปลี่ยนค่าไม่ได้
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
// // number,string,boolean จะเป็น type ของ ตัวแปล
// let aString: string = "สตริงนะครับ";
// let aNumber: number = 0;
// let aDouble: number = 17.6;
// let aBool: boolean = true;
// //if,else,switch case,for(foreach)
// function ifelseCheck(studentId: number): boolean {
//   // if(studentId === 1){
//   //     return false
//   // }else{
//   //     return true
//   // }
//   // if(studentId === 1)
//   //     return false
//   let checkBool: boolean = studentId === 1 ? false : true;
//   return checkBool;
// }
// console.log("ifelsecheck : ", ifelseCheck(11));
// function switchCheck(a: string): string {
//   switch (a) {
//     case "1":
//       return "true";
//     case "2":
//       return "false";
//   }
//   return "Test 1234";
// }
// console.log(switchCheck("111"));
// function forCheck(): any {
//   let arrObj: any = [
//     {
//       id: 1,
//       name: "ratchaphoom",
//     },
//     {
//       id: 2,
//       name: "seesu",
//     },
//     {
//       id: 3,
//       name: "ronaldo",
//     },
//   ];
//   // for(var i:number = 0;i < arrObj.length ; i++){
//   //     console.log(arrObj[i].name)
//   // }
//   arrObj.forEach((res: any, i: number) => {
//     console.log(i + 1 + " " + res.name);
//   });
// }
// forCheck();
// // filter,map(change value)
// function filterCheck(): any {
//   let arrObj: any = [
//     {
//       id: 1,
//       name: "ratchaphoom",
//     },
//     {
//       id: 2,
//       name: "seesu",
//     },
//     {
//       id: 3,
//       name: "ronaldo",
//     },
//   ];
//   let subObj: any = [
//     {
//       id: 1,
//       name: "2020",
//       sub: 1,
//     },
//     {
//       id: 2,
//       name: "2021",
//       sub: 1,
//     },
//     {
//       id: 3,
//       name: "2021",
//       sub: 3,
//     },
//   ];
//   // filter //
//   let chk: any = arrObj.filter((res: any) => res.name === "ronaldo");
//   console.log(JSON.stringify(chk));
//   // map change value //
//   let chkMap: any = chk.map((res: any) => ({
//     ...res,
//     name: "messi",
//   }));
//   console.log(JSON.stringify(chkMap));
//   let mergeArr: any = arrObj
//     .filter((res: any) => res.name === "seesu")
//     .map((res: any) => ({
//       ...res,
//       name: "tuktuk",
//     }));
//   console.log(mergeArr);
//   // nav bar sub menu
//   let trymergeArr: any = arrObj
//     .filter((res: any) => res.name === "ratchaphoom")
//     .map((res: any) => ({
//       ...res,
//       name: JSON.stringify(subObj.filter((x: any) => res.id === x.sub)),
//     }));
//   console.log(trymergeArr);
// }
// filterCheck();
function classRoom_assignment() {
    var teacher = [
        {
            id: 1,
            name: "pensee",
        },
        {
            id: 2,
            name: "somyod",
        },
    ];
    var student = [
        {
            id: 1,
            name: "manee",
            subteacher: 1,
            sub_work: [
                {
                    workid: 1,
                },
                {
                    workid: 2,
                },
            ],
        },
        {
            id: 2,
            name: "mana",
            subteacher: 1,
            sub_work: [
                {
                    workid: 1,
                },
            ],
        },
        {
            id: 3,
            name: "bas",
            subteacher: 1,
            sub_work: [
                {
                    workid: 1,
                },
            ],
        },
        {
            id: 4,
            name: "bell",
            subteacher: 2,
            sub_work: [
                {
                    workid: 1,
                },
            ],
        },
        {
            id: 5,
            name: "nadate",
            subteacher: 2,
            sub_work: [
                {
                    workid: 1,
                },
                {
                    workid: 2,
                },
            ],
        },
        {
            id: 6,
            name: "yaya",
            subteacher: 2,
            sub_work: [
                {
                    workid: 1,
                },
            ],
        },
    ];
    var student_work = [
        {
            id: 1,
            workname: "ภาษาไทย",
        },
        {
            id: 2,
            workname: "ภาษาอังกฤษ",
        },
    ];
    var teacherMap = teacher.map(function (res) { return (__assign(__assign({}, res), { studentGroup: student
            .filter(function (s) { return res.id === s.subteacher; })
            .map(function (it, i) { return (__assign({}, it)); }) })); });
    teacherMap.forEach(function (res, i) {
        var keepMerge = [];
        res.studentGroup.forEach(function (xx, xn) {
            var aa = [];
            xx.sub_work.forEach(function (al) {
                var ee = student_work.filter(function (ae) { return al.workid === ae.id; });
                aa.push(ee);
            });
            var ma = __assign(__assign({}, xx), { work_name: aa });
            keepMerge.push(ma);
        });
        var finish_m = __assign(__assign({}, res), { studentGroup: JSON.stringify(keepMerge) });
        console.log(finish_m);
    });
}
classRoom_assignment();
